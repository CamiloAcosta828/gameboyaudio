class AudioPlayer {
    constructor (src, play) {
        this.src = src;
        this.audio = new Audio(this.src);
        this.controls = {
            play
        }
    }

}
